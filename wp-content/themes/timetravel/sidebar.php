<?php
/*
* Theme Name: Time Travel - Timeline WordPress Theme
* Theme Author: Andrey Boyadzhiev / flasherland.com
*
* Description: Time Travel Wordpress Theme is a next generation 
* website, developed both on the edge of technology and design. 
* The built-in voice control makes it both revolutionary and at the same time super intuitive to use. 
*
* Version: 1.0 
*/
?>
<div class="sidebar">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar() ) : else : ?>

<?php endif; ?>
</div>