<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_outback');

/** MySQL database username */
define('DB_USER', 'coipl-demo-user');

/** MySQL database password */
define('DB_PASSWORD', 'coipl123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(uaHQHS,V^Q$N%!9MhnRO[k LXEYKosVGb]S-]rT;XMNC<I+H!~^$~B29N[K)^HJ');
define('SECURE_AUTH_KEY',  'a0TH9o;N;=l8<.&NK:(ErUe*91<f+z&fmCK>LD@d<>NIP3Q9M+VcgqX(TBNA(`,G');
define('LOGGED_IN_KEY',    'yD_H&,,d({++,K@hk/R/i2L{{FZK?vi8++[|g5-~<wM-lR8Y@=&e-?+Xk70sDO*-');
define('NONCE_KEY',        'n5kPI/`Af/QwB;W)CDo5>|k$-sx8|Ii2Aqm%0 lHN*ujcjc{6M%S+@h )y$SZ)$u');
define('AUTH_SALT',        'w:J:*war,jojXS-%kRIB9A.e_zq`<NrSYb2KsFeJSqXgBG@t73L-2ZS7<`:S-H3+');
define('SECURE_AUTH_SALT', '|vfD#|oi.IU9];si>sU=*j9&++Y;Y?B1]t|>K%..iNg@pF3mX-^lo/} euCu2}Y)');
define('LOGGED_IN_SALT',   'EKVd%lIG5+C&mtFdyIw++J`U&xk;.]%@!|Y{0Fd%Ytu^orY0k_}J`8%*,-[@0>)$');
define('NONCE_SALT',       ';s[4?MuAYI_qTN`J%c -z8hCgHTU+NF>Z>W$81 -~Pa[jB^ U`4e |G?yI(q_N-,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ob_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
